﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SharpNes.Core.Test
{
    [TestClass]
    public class CpuTest
    {
        [TestMethod]
        public void CanReadFromMemory()
        {
            var cpu = new Cpu();
            cpu.Memory.WriteMemory8Bit(0xFF11, 0x10);
            var result = cpu.Memory.ReadMemory8Bit(0xFF11);

            Assert.AreEqual(0x10, result);
        }
    }
}

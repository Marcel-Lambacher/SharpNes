﻿namespace SharpNes.Core
{
    /// <summary>
    /// Represents the registers used for the 6502.
    /// </summary>
    public class CpuRegisters
    {
        /// <summary>
        /// Register for arthmetic operations.
        /// </summary>
        public byte AccumulatorRegister { get; set; }

        /// <summary>
        /// Index register for general use.
        /// </summary>
        public byte XRegister { get; set; }

        /// <summary>
        /// Index register for general use.
        /// </summary>
        public byte YRegister { get; set; }

        /// <summary>
        /// Stores the current location in the memory.
        /// </summary>
        public ushort ProgrammCounter { get; set; }

        /// <summary>
        /// Stack pointer which points of stores locations of the memory. 
        /// Uses for subroutines, branches, pushes...
        /// </summary>
        public byte StackPointer { get; set; }
    }
}

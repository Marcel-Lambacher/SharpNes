﻿namespace SharpNes.Core
{
    /// <summary>
    /// Represents the status flags for the 6502 processor.
    /// (Some flags are removed, because the nes doesn't use those.)
    /// </summary>
    public class StatusFlags
    {
        /// <summary>
        /// Carry flag will be true, if last addition or shift resuled in a carry, or ig
        /// last substraction resulted in no borrow.
        /// </summary>
        public bool CarryFlag { get; set; }

        /// <summary>
        /// Zero flag will be true, if last operation resulted with a 0 value.
        /// </summary>
        public bool ZeroFlag { get; set; }

        /// <summary>
        /// Disables or enables interrupts.
        /// </summary>
        public bool InterruptFlag { get; set; }

        /// <summary>
        /// Will be true if last ADC or SBC resulted in signed overflow, or D6 form last BIT.
        /// </summary>
        public bool OverflowFlag { get; set; }

        /// <summary>
        /// Will be true if a result of an operation is negative, cleared if positive.
        /// </summary>
        public bool NegativeFlag { get; set; }
    }
}

﻿namespace SharpNes.Core
{
    /// <summary>
    /// Represents the addressing modes for each operation code. With a addressing mode, you can identify
    /// how operands are passed to a operation code.
    /// </summary>
    public enum AddressMode
    {
        /// <summary>
        /// These instructions targeting the accumulator register.
        /// No operands required. 
        /// Length: 1 byte
        /// </summary>
        Accumulator,

        /// <summary>
        /// These instruction have their operand defined next the operation code.
        /// Length: 2 byte
        /// Example: ORA #$B2
        /// </summary>
        Immediate,

        /// <summary>
        /// These instructions doesn't need any operand.
        /// This mode is used for operations like CLC -> clear carry flag
        /// Length: 1 byte
        /// </summary>
        Implied,

        /// <summary>
        /// Relative instructions are only used for branch operations.
        /// The byte after the operation code is the branch offset. The new address will be PC + offset.
        /// The offset is a signed byte, means + 127 or - 127.
        /// If bit 7 is 1, the bytes 0 - 6 represents a negativ number.
        /// If bit 7 is 0, the bytes 0 - 6 represents a positiv number.
        /// This instructions will also check the zero flag.
        /// Length: 2 bytes
        /// </summary>
        Relative,

        /// <summary>
        /// These instructions will fetch a value out of the ram. The value are 2 byte big and followed by the operation code.
        /// Length: 3 bytes
        /// </summary>
        Absolute,

        /// <summary>
        /// The first 256 bytes in the ram are called zero page. Instruction with a zero page mode will have a 1 byte operand.
        /// With this operand, the instruction can access the zero page ram.
        /// Length: 2 bytes
        /// </summary>
        ZeroPage,

        /// <summary>
        /// This address mode is only used by JMP.
        /// The next 2 bytes after the operation code are the absolute address in the memory.
        /// The value of this address will be assigned to the PC.
        /// </summary>
        Indirect,

        /// <summary>
        /// With this addressing mode you enumerate the memory very simple.
        /// The next 2 bytes after the operation code represents the base address. Then the value of register X or Y will be
        /// added to this base address. With the result of this, you are able to simply enumerate the memory.
        /// Length: 3 bytes
        /// </summary>
        AbsoluteIndexed,

        /// <summary>
        /// This wirks just like the aboslute indexed, but the target address is limited to the fhirst 0xFF bytes (zero page)
        /// The target address will wrap arround and will always be in the zero page.
        /// Length: 2 bytes
        /// </summary>
        ZeroPageIndexed,

        /// <summary>
        /// This mode is only used by the X register.
        /// The 1-byte address after the operation code represent a zero page address.
        /// This address will be added with the value in the X register.
        /// Length: 2 bytes
        /// </summary>
        IndexedIndirect,

        /// <summary>
        /// This mode is only used by the Y register.
        /// The 1-byte address after the operation code represent a zero page address.
        /// First, the value in the zero page will fetched and added with the value of the Y register.
        /// Length: 2 bytes
        /// </summary>
        IndirectIndexed
    }
}

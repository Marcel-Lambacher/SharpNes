﻿using System;

namespace SharpNes.Core
{
    /// <summary>
    /// Represents an operation code attribute
    /// </summary>
    public class OperationCodeAttribute: Attribute
    {
        /// <summary>
        /// The current operation code of the executing instruction.
        /// </summary>
        public byte OperationCode { get; private set; }

        /// <summary>
        /// The used addressing mode of the executing instruction.
        /// </summary>
        public AddressMode AddressMode { get; private set; }

        /// <summary>
        /// The assembler string of the executing instruction.
        /// This will be used from the nes debugger.
        /// </summary>
        public string Assembler { get; private set; }

        /// <summary>
        /// Initializes a new instance of the operation code attribute.
        /// </summary>
        /// <param name="opCode">The current operation code of the executing instruction.</param>
        /// <param name="addressMode">he used addressing mode of the executing instruction.</param>
        /// <param name="assembler">The assembler string of the executing instruction.</param>
        public OperationCodeAttribute(byte opCode, AddressMode addressMode, string assembler)
        {
            OperationCode = opCode;
            AddressMode = addressMode;
            Assembler = assembler;
        }
    }
}

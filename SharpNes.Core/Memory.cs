﻿using System;

namespace SharpNes.Core
{
    /// <summary>
    /// Represents the memory used by the nes
    /// </summary>
    public class Memory
    {
        private byte[] _memory;

        public Memory()
        {
            //16-bit memory size equals 65535 bytes
            _memory = new byte[0xFFFF];
        }

        public byte ReadMemory8Bit(ushort address)
        {
            //Convert address into a little endian number
            var realAddress = ((address & 0x00FF) << 8) | ((address & 0xFF00) >> 8);
            return _memory[realAddress];
        }

        public ushort ReadMemory16Bit(ushort address)
        {
            throw new NotImplementedException();
        }

        public void WriteMemory8Bit(ushort address, byte value)
        {
            //Convert address into a little endian number
            var realAddress = ((address & 0x00FF) << 8) | ((address & 0xFF00) >> 8);
            _memory[realAddress] = value;
        }

        public void WriteMemory16Bit(ushort address, ushort value)
        {
            throw new NotImplementedException();
        }
    }
}

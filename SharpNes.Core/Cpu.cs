﻿using System;
using System.Linq;
using System.Reflection;

namespace SharpNes.Core
{
    /// <summary>
    /// Represents the 6502 cpu which is used by the nes.
    /// </summary>
    public class Cpu
    {
        /// <summary>
        /// Represents the registers used for the 6502.
        /// </summary>
        public CpuRegisters Registers { get; private set; }

        /// <summary>
        /// Represents the status flags for the 6502 processor.
        /// </summary>
        public StatusFlags StatusFlags { get; private set; }

        /// <summary>
        /// Represents the memory used by the 6502 cpu
        /// </summary>
        public Memory Memory { get; private set; }

        /// <summary>
        /// Represents all instructions, accessing by the operation code.
        /// Returns the needed cpu cycles for clock synchronisation.
        /// </summary>
        private Func<int>[] _instructionTable;

        public Cpu()
        {
            Initialize();
        }

        public void NextStep()
        {
            var operationCode = NextOpCode();
            var instruction = _instructionTable[operationCode];

            if (instruction == null)
            {
                Console.WriteLine("Operation code: 0x" + operationCode.ToString("X") + " not supported or implemented.");
                return;
            }

            var cycles = instruction();
            //TODO: Cycle accurate clock speed synchronisation
        }

        /// <summary>
        /// Initializes the nes cpu
        /// </summary>
        private void Initialize()
        {
            Registers = new CpuRegisters();
            StatusFlags = new StatusFlags();
            Memory = new Memory();

            InitializeInstructionTable();
        }

        /// <summary>
        /// Retrieve all methods which has the attribute <c>OperationCodeAttribute</c>.
        /// These methods are used to build the instruction table.
        /// </summary>
        private void InitializeInstructionTable()
        {
            //256 operation codes
            const int instructionCodeCount = 0xFF;
            _instructionTable = new Func<int>[instructionCodeCount];

            var methods = typeof (Cpu).GetMethods(BindingFlags.Instance | BindingFlags.NonPublic).Where(method => method.GetCustomAttribute<OperationCodeAttribute>() != null);
            foreach (var methodInfo in methods)
            {
                var operationCode = methodInfo.GetCustomAttribute<OperationCodeAttribute>().OperationCode;
                _instructionTable[operationCode] = Delegate.CreateDelegate(typeof(Func<int>), this, methodInfo) as Func<int>;
            }
        }

        private byte NextOpCode()
        {
            var operationCode = Memory.ReadMemory8Bit(Registers.ProgrammCounter);
            Registers.ProgrammCounter += 1;
            return operationCode;
        }
    }
}
